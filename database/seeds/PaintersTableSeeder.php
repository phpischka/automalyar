<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PaintersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Painter::class, 50)->create();
    }
}
