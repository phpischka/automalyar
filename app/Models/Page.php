<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;
    
    //for search
    public const prefixLink = '/';
    public const getLink = 'slug';
    public const getLinkText = 'title';
    public const searchBy = 'title';
    
}
