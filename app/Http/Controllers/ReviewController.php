<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Review;

class ReviewController extends Controller
{
    public function index()
    {
        $page = Page::where('slug', 'otzyvy')->first();
        
        if (!$page) {
            return abort(404);
        }
        
        $reviews = Review::orderBy('order')->get();
        
        return view('reviews', compact('page', 'reviews'));
        
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'description' => 'required|max:1000',
        ]);
            $master_id = $request->input('id');
            $rating = $request->input('rating');
            $name = $request->input('name');
            $email = $request->input('email');
            $description = $request->input('description');
            $master = $request->input('master');
        try {
            $review = new Review();
            $review->ip_adress = $request->ip();
            
            if ($master == 'painter') {
                $review->painter_id = $master_id;
            }elseif ($master == 'picker') {
                $review->picker_id = $master_id;
            } else {
                return false;
            }
            
            $review->rating = $rating;
            $review->name = $name;
            $review->email = $email;
            $review->description = $description;
            $review->save();
            
        } catch (\Exception $e) {
            return json_encode(['status' => 401]);
        }
        
        $reviews = Review::where($master.'_id', $master_id)->get();
        
        $numberReviews = $reviews->count();
        
        $middleStarNum = round($reviews->avg('rating'), 1);
        
        $middleStar = floor($reviews->avg('rating'));
        
        $data = [
            'status' => 200,
            'id' => $review->id,
            'rating' => $review->rating,
            'name' => $review->name,
            'description' => $review->description,
            'created_at' => $review->created_at->format('j, F, Y, H : i'),
            'numberReviews' => $numberReviews,
            'middleStarNum' => $middleStarNum,
            'middleStar' => $middleStar,
            ];
        
        return json_encode($data);
    }
}
