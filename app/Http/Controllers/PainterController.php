<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewPainterRegistration;
use App\Models\Painter;
use App\Models\Category;

class PainterController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $user_id = Auth::user()->id;
        $entry = Painter::where('user_id', $user_id)->first();

        //if isset entry from user
        if ($entry) {
            $entry = $entry;
        } else {
            $entry = false;
        }
        
        $parent_category = Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {
            return redirect('/cabinet');
        }

        $categories = $parent_category->children()->orderBy('order')->get();

        return view('admin.cabinet', compact('entry', 'user_id', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $user = Auth::user();
        $entry = Painter::where('user_id', $user->id)->first();

        //if isset entry from user
        if ($entry) {
            return redirect('/cabinet/advt/edit/' . $entry->id);
        } else {

            $parent_category = Category::where('slug', 'kuzov')->first();

            if (!$parent_category) {
                return redirect('/cabinet');
            }

            $categories = $parent_category->children()->orderBy('order')->get();


            return view('admin.advertisement_create', compact('user', 'categories' ));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'company' => 'required|max:255',
            'about' => 'required|max:255',
            'country' => 'required|max:255',
            'area' => 'required|max:255',
            'city' => 'required|max:255',
            'street' => 'required|max:255',
            'house' => 'required|max:255',
            'phone' => 'required|max:255',
            'email' => 'required|max:255|email',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //service
            'bamper_front' => 'max:255',
            'hood' => 'max:255',
            'wing_front' => 'max:255',
            'roof' => 'max:255',
            'door_front' => 'max:255',
            'door_rear' => 'max:255',
            'trunk_lid' => 'max:255',
            'wing_rear' => 'max:255',
            'bumper_rear' => 'max:255',
            'part_local' => 'max:255',
            'threshold' => 'max:255',
            'mirror' => 'max:255',
            'car_full' => 'max:255',
        ]);
        
        $ip_adress = $request->ip();
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $company = $request->input('company');
        $about = $request->input('about');
        $site = $request->input('site');
        $country = $request->input('country');
        $area = $request->input('area');
        $city = $request->input('city');
        $street = $request->input('street');
        $house = $request->input('house');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $image = $request->file('image');
        
        if ($image) {
            $path_image = ImageController::store($image);
        }
        
        //service
        $bamper_front = $request->input('bamper_front');
        $hood = $request->input('hood');
        $wing_front = $request->input('wing_front');
        $roof = $request->input('roof');
        $door_front = $request->input('door_front');
        $door_rear = $request->input('door_rear');
        $trunk_lid = $request->input('trunk_lid');
        $wing_rear = $request->input('wing_rear');
        $bumper_rear = $request->input('bumper_rear');
        $part_local = $request->input('part_local');
        $threshold = $request->input('threshold');
        $mirror = $request->input('mirror');
        $car_full = $request->input('car_full');

        try {

            $painter = new Painter();
            $painter->ip_adress = $ip_adress;
            $painter->status = 1;
            $painter->first_name = $first_name;
            $painter->last_name = $last_name;
            $painter->company = $company;
            $painter->about = $about;
            $painter->site = $site;
            $painter->country = $country;
            $painter->area = $area;
            $painter->city = $city;
            $painter->street = $street;
            $painter->house = $house;
            $painter->email = $email;
            $painter->phone = $phone;
            
            if (isset($path_image)) {
                $painter->image = $path_image;
            }
            
            //service
            $painter->bamper_front = $bamper_front;
            $painter->hood = $hood;
            $painter->wing_front = $wing_front;
            $painter->roof = $roof;
            $painter->door_front = $door_front;
            $painter->door_rear = $door_rear;
            $painter->trunk_lid = $trunk_lid;
            $painter->wing_rear = $wing_rear;
            $painter->bumper_rear = $bumper_rear;
            $painter->part_local = $part_local;
            $painter->threshold = $threshold;
            $painter->mirror = $mirror;
            $painter->car_full = $car_full;

            $painter->save();
            
              $feedback = [
              'current' => 'created',
              'first_name' => $first_name,
              'last_name' => $last_name,
              'company' => $company,
              'email' => $email,
              'phone' => $phone,
              'site' => $site,
              
              ];
              Mail::to(explode(', ', setting('site.request_emails')))->send(new NewPainterRegistration($feedback));

        } catch (\Exception $e) {
            return \Redirect::back()->with('error', 'Данные не сохранились обратитесь в поддержку');
        }
        return redirect('/cabinet/advt/edit/' . $painter->id)->with('success', 'Данные успешно сохранены!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = Auth::user();
        $entry = Painter::where('user_id', $user->id)->first();

        //if isset entry from user
        if (!$entry) {
            return redirect('/cabinet/advt/create');
        }

        $parent_category = Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {
            return redirect('/cabinet');
        }

        $categories = $parent_category->children()->orderBy('order')->get();

        return view('admin.advertisement_edit', compact('user', 'categories', 'entry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'company' => 'required|max:255',
            'about' => 'required|max:255',
            'country' => 'required|max:255',
            'area' => 'required|max:255',
            'city' => 'required|max:255',
            'street' => 'required|max:255',
            'house' => 'required|max:255',
            'phone' => 'required|max:255',
            'email' => 'required|max:255|email',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //service
            'bamper_front' => 'max:255',
            'hood' => 'max:255',
            'wing_front' => 'max:255',
            'roof' => 'max:255',
            'door_front' => 'max:255',
            'door_rear' => 'max:255',
            'trunk_lid' => 'max:255',
            'wing_rear' => 'max:255',
            'bumper_rear' => 'max:255',
            'part_local' => 'max:255',
            'threshold' => 'max:255',
            'mirror' => 'max:255',
            'car_full' => 'max:255',
        ]);
        
        $ip_adress = $request->ip();
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $company = $request->input('company');
        $about = $request->input('about');
        $site = $request->input('site');
        $country = $request->input('country');
        $area = $request->input('area');
        $city = $request->input('city');
        $street = $request->input('street');
        $house = $request->input('house');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $image = $request->file('image');
        
        if ($image) {
            $path_image = ImageController::store($image);
        }
            
        //service
        $bamper_front = $request->input('bamper_front');
        $hood = $request->input('hood');
        $wing_front = $request->input('wing_front');
        $roof = $request->input('roof');
        $door_front = $request->input('door_front');
        $door_rear = $request->input('door_rear');
        $trunk_lid = $request->input('trunk_lid');
        $wing_rear = $request->input('wing_rear');
        $bumper_rear = $request->input('bumper_rear');
        $part_local = $request->input('part_local');
        $threshold = $request->input('threshold');
        $mirror = $request->input('mirror');
        $car_full = $request->input('car_full');

        try {
            $painter = Painter::where('id', $id)->first();
            $painter->ip_adress = $ip_adress;
            $painter->first_name = $first_name;
            $painter->last_name = $last_name;
            $painter->company = $company;
            $painter->about = $about;
            $painter->site = $site;
            $painter->country = $country;
            $painter->area = $area;
            $painter->city = $city;
            $painter->street = $street;
            $painter->house = $house;
            $painter->email = $email;
            $painter->phone = $phone;
            
            if (isset($path_image)) {
                $painter->image = $path_image;
            }
        
            //service
            $painter->bamper_front = $bamper_front;
            $painter->hood = $hood;
            $painter->wing_front = $wing_front;
            $painter->roof = $roof;
            $painter->door_front = $door_front;
            $painter->door_rear = $door_rear;
            $painter->trunk_lid = $trunk_lid;
            $painter->wing_rear = $wing_rear;
            $painter->bumper_rear = $bumper_rear;
            $painter->part_local = $part_local;
            $painter->threshold = $threshold;
            $painter->mirror = $mirror;
            $painter->car_full = $car_full;

            $painter->save();
            
            
              $feedback = [
              'current' => 'edited',
              'first_name' => $first_name,
              'last_name' => $last_name,
              'company' => $company,
              'email' => $email,
              'phone' => $phone,
              'site' => $site,
               
              
              ];
              Mail::to(explode(', ', setting('site.request_emails')))->send(new NewPainterRegistration($feedback));

        } catch (\Exception $e) {
            return \Redirect::back()->with('error', 'Данные не сохранились обратитесь в поддержку');
        }
        return redirect('/cabinet/advt/edit/' . $painter->id)->with('success', 'Данные успешно сохранены!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
    

}
