<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Category;
use App\Models\Painter;
use App\Models\Post;
use App\Models\Review;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Support\Collection;

class PageController extends Controller
{
    public const DEFAULT_AREA = 1;
    public const DEFAULT_CITY = 1;
    public const DEFAULT_MINUTE = 5;
    public const DEFAULT_PAGINATE = 10;
    
    public const DEFAULT_CATEGORY_DETAIL = 'bamper-perednij';

    public function index()
    {
        $page = Page::where('slug', '/')->first();
        
        if (!$page) {
            return abort(404);
        }
        
        $parent_category = Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {
            $categories = false;
        }else{
            $categories = $parent_category->children()->orderBy('order')->get();
        } 
        
        $painters = Painter::where('status', '1')->get();
        
        return view('homepage', compact('page', 'categories', 'painters'));
        
    }
    
    public function painting(Request $request)
    {
        $slug = self::DEFAULT_CATEGORY_DETAIL;
        
        $paginatePage = $request->page;
        
        $area = $request->area;
        
        $city = $request->city;
        
        $price = $request->price;
        
        $rating = $request->rating ;
        
        $minutes = Carbon::now()->addMinutes(self::DEFAULT_MINUTE);
        
        if ($rating === 'asc' || $rating === 'desc' ) {
            $rating = $rating;
        } else {
            $rating = false;
        }
        
        if ($price === 'asc' || $price === 'desc' ) {
            $price = $price;
        } else {
            $price = false;
        }
        
        $page = Page::where('slug', 'pokraska')->first();
        
        $posts = Post::where('status', 'PUBLISHED')->latest('created_at')->limit(3)->get();
        
        if (!$page) {
            return abort(404);
        }
        
        //текущая категория
        $category_detail = Category::where('slug', $slug)->first();
        
        if (!$category_detail) {
            return abort(404);
        }
        
        $sort_field = $category_detail->input_name_form;
        
        $paint_detail = $sort_field;
        
        if ($paginatePage && $paginatePage !== '') {
            $paginatePage = $paginatePage;
        }else{
            $paginatePage = 1;
        }
        
        $painters = Cache::remember('pokraska/' . $slug . '?page=' . $paginatePage . '&area=' . $area . '&city=' . $city . '&price=' . $price . '&rating=' . $rating, $minutes, function () use ($paint_detail, $rating, $city, $price, $sort_field){
           return Painter::where('status', '1')
                ->where($paint_detail, '>=', 100)
                ->when($rating, function ($query) use ($rating) {
                    return $query
                            ->leftJoin("reviews", "painters.id", "=", "reviews.painter_id")
                            ->select(DB::raw('painters.*'))
                            ->groupByRaw('painters.id, painters.user_id, painters.status, '
                                    . 'painters.vip, painters.tariff, painters.ip_adress, painters.first_name, '
                                    . 'painters.last_name, painters.email, painters.phone, painters.company, painters.about, '
                                    . 'painters.site, painters.country, painters.area, painters.city, '
                                    . 'painters.street, painters.house, painters.image, painters.phone_view, '
                                    . 'painters.bamper_front, painters.hood, painters.wing_front, painters.roof, '
                                    . 'painters.door_front, painters.door_rear, painters.trunk_lid, painters.wing_rear, '
                                    . 'painters.bumper_rear, painters.threshold, painters.mirror, painters.part_local, '
                                    . 'painters.car_full, painters.created_at, painters.updated_at')
                            ->orderByRaw('AVG(reviews.rating)' .$rating);
                })
                ->when($city, function ($query) use ($city) {
                    return $query->where('city', $city);
                })      
                ->when($price, function ($query) use ($price, $sort_field) {
                    return $query->orderBy($sort_field, $price);
                })
                ->paginate(self::DEFAULT_PAGINATE);
        });

        $parent_category = Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {
            $categories = false;
        }else{
            $categories = $parent_category->children()->orderBy('order')->get();
        } 
        
        if ($area) {
            $area = $area;
        }else{
            $area = self::DEFAULT_AREA;
        }
        
        if ($city) {
            $city = $city;
        }else{
            $city = self::DEFAULT_CITY;
        }
        
        
        return view('painting', compact('page', 'posts', 'category_detail', 'categories', 'painters', 'area', 'city', 'price', 'rating'));
        
    }
    
    public function painting_category(Request $request, $slug)
    {
        $paginatePage = $request->page;
        
        $area = $request->area;
        
        $city = $request->city;
        
        $price = $request->price;
        
        $rating = $request->rating ;
        
        $minutes = Carbon::now()->addMinutes(self::DEFAULT_MINUTE);
        
        if ($rating === 'asc' || $rating === 'desc' ) {
            $rating = $rating;
        } else {
            $rating = false;
        }
        
        if ($price === 'asc' || $price === 'desc' ) {
            $price = $price;
        } else {
            $price = false;
        }
        
        //текущая Page категория
        $page = Category::where('slug', $slug)->first();
        
        if (!$page) {
            return abort(404);
        }
        
        $category_detail = $page;
        
        $sort_field = $category_detail->input_name_form;
        
        $paint_detail = $sort_field;
        
        $posts = Post::where('status', 'PUBLISHED')->latest('created_at')->limit(3)->get();
        
        if (!$page) {
            return abort(404);
        }   
        
        if ($paginatePage && $paginatePage !== '') {
            $paginatePage = $paginatePage;
        }else{
            $paginatePage = 1;
        }
        
        $painters = Cache::remember('pokraska/' . $slug . '?page=' . $paginatePage . '&area=' . $area . '&city=' . $city . '&price=' . $price . '&rating=' . $rating, $minutes, function () use ($paint_detail, $rating, $city, $price, $sort_field){
            return Painter::where('status', '1')
                ->where($paint_detail, '>=', 100)
                ->when($rating, function ($query) use ($rating) {
                    return $query
                            ->leftJoin("reviews", "painters.id", "=", "reviews.painter_id")
                            ->select(DB::raw('painters.*'))
                            ->groupByRaw('painters.id, painters.user_id, painters.status, '
                                    . 'painters.vip, painters.tariff, painters.ip_adress, painters.first_name, '
                                    . 'painters.last_name, painters.email, painters.phone, painters.company, painters.about, '
                                    . 'painters.site, painters.country, painters.area, painters.city, '
                                    . 'painters.street, painters.house, painters.image, painters.phone_view, '
                                    . 'painters.bamper_front, painters.hood, painters.wing_front, painters.roof, '
                                    . 'painters.door_front, painters.door_rear, painters.trunk_lid, painters.wing_rear, '
                                    . 'painters.bumper_rear, painters.threshold, painters.mirror, painters.part_local, '
                                    . 'painters.car_full, painters.created_at, painters.updated_at')
                            ->orderByRaw('AVG(reviews.rating)' .$rating);
                })
                ->when($city, function ($query) use ($city) {
                    return $query->where('city', $city);
                })      
                ->when($price, function ($query) use ($price, $sort_field) {
                    return $query->orderBy($sort_field, $price);
                })
                ->paginate(self::DEFAULT_PAGINATE);
        });
                
        $parent_category = Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {
            $categories = false;
        }else{
            //категории меню слева
            $categories = $parent_category->children()->orderBy('order')->get();
        } 
        
        if ($area) {
            $area = $area;
        }else{
            $area = self::DEFAULT_AREA;
        }
        
        if ($city) {
            $city = $city;
        }else{
            $city = self::DEFAULT_CITY;
        }
        
        return view('painting_category', compact('page', 'posts', 'category_detail', 'categories', 'painters', 'area', 'city', 'price', 'rating'));
        
    }
    
    public function blog()
    {
        $page = Page::where('slug', 'blog')->first();
        
        if (!$page) {
            return abort(404);
        }
        
        $posts = Post::where('status', 'PUBLISHED')->paginate(10);
        
        return view('blog', compact('page', 'posts'));  
    }
    
    public function post($slug)
    {
        $page = Post::where('slug', $slug)->where('status', 'PUBLISHED')->first();
        
        if (!$page) {
            return abort(404);
        }
        
        $comments = $page->comments()->where('status', 1)->get();
        
        $current_url = url()->current();
        
        return view('post', compact('page', 'comments', 'current_url'));  
    }
    
    public function terms()
    {
        $page = Page::where('slug', 'usloviya-ispolzovaniya')->first();
        
        if (!$page) {
            return abort(404);
        }    
        
        return view('terms', compact('page'));  
    }
    public function painter($id)
    {
        $painter = Painter::where('id', $id)->first();
        
        if (!$painter) {
            return abort(404);
        }
        
        $area = $painter->area;
        
        $city = $painter->city;
        
        $parent_category = Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {
            $categories = false;
        }else{
            
            $categories = $parent_category->children()->orderBy('order')->get();
        } 
        
        $reviews = Review::where('painter_id', $painter->id)->get();
        
        $numberReviews = $reviews->count();
        
        $middleStarNum = round($reviews->avg('rating'), 1);
        
        $middleStar = floor($reviews->avg('rating'));
     
        
        return view('painter', compact('painter', 'categories', 'reviews', 'numberReviews', 'middleStarNum', 'middleStar', 'area', 'city'));  
    }
    
    public function review_painter($id)
    {
        $painter = Painter::where('id', $id)->first();
        
        if (!$painter) {
            return abort(404);
        }
        
        $reviews = $painter->reviews;
        
        $numberReviews = $reviews->count();
        
        $middleStarNum = round($reviews->avg('rating'), 1);
        
        $middleStar = floor($reviews->avg('rating'));
        
        return view('reviews', compact('painter', 'reviews', 'numberReviews', 'middleStarNum', 'middleStar')); 
    }
    
    public function getPhone(Request $request) 
    {
        $masterName = $request->master;
        $id = $request->id;
        
        if ($masterName == 'painter') {
            $master = Painter::where('id', $id)->first();
        }
      
        if (!$master) {
            return json_encode(['status' => 401]);
        }
        
        $master->phone_view++;
        
        $master->save();
        
        $data = [
            'status' => 200,
            'phone' => $master->phone,
            ];
        
        return json_encode($data);
    }
    
    public function search(Request $request) 
    {
        $request->validate([
            'query' => 'string',
        ]);
        
        $query = $request->input('query');
        
        //Модели по каким производить поиск
        $modelsSearch = ['Page', 'Painter', 'Post'];
        
        $links = \Search::searchPagesByQuery($modelsSearch, $query);
        
        $collectLinks = collect($links);
        
        // convert array to collection with pagination
        $results = (new Collection($collectLinks))->paginate(self::DEFAULT_PAGINATE);
        
        return view('search', compact('results')); 
       
        
    }
}
