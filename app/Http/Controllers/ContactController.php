<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Contact;
use App\Models\Painter;
use App\Jobs\SendContactEmail;

class ContactController extends Controller
{
    public function send(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'phone' => 'required|max:255',
            'message' => 'required',
        ]);
            $name = $request->input('name');
            $email = $request->input('email');
            $phone = $request->input('phone');
            $message = $request->input('message');
        try {
            $contact = new Contact();
            $contact->ip_adress = $request->ip();
            $contact->name = $name;
            $contact->email = $email;
            $contact->phone = $phone;
            $contact->message = $message;
            $contact->save();
            
            $emails = setting('site.request_emails');
            
            $feedback = [
                'name' => $name,
                'email' => $email, 
                'phone' => $phone,
                'message' => $message,
            ];
            
            $job = (new SendContactEmail($emails, $feedback))
                ->delay(Carbon::now()->addMinutes(1));
            
            dispatch($job);
            
        } catch (\Exception $e) {
            return json_encode(['status' => 401]);
        }
        
        return json_encode(['status' => 200]); 
    }
    
    public function send_partner(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'phone' => 'required|max:255',
            'message' => 'required',
        ]);
        
        try {
            $entry = Painter::where('id', $request->input('id'))->first();
            
            $emails = setting('site.request_emails') .', '. $entry->email;
            
            $feedback = [
                'name' => $request->input('name'),
                'email' => $request->input('email'), 
                'phone' => $request->input('phone'),
                'message' => $request->input('message'),
            ];
            $job = (new SendContactEmail($emails, $feedback))
                ->delay(Carbon::now()->addMinutes(1));
            
            dispatch($job);
            
        } catch (\Exception $e) {
            return json_encode(['status' => 401]);   
        }
        
        return json_encode(['status' => 200]); 
        
    }
  

}