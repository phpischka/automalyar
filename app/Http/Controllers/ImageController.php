<?php

namespace App\Http\Controllers;
use Image;

class ImageController extends Controller
{
    public static function store($image) {

        $input['imagename'] = time().'.'.$image->extension();

        $destinationPath = public_path('/storage/uploads');

        $img = Image::make($image->path());

        $img->fit(400)->save($destinationPath.'/'.$input['imagename']);
        
        return 'uploads/'.$input['imagename'];
        
    }
}
