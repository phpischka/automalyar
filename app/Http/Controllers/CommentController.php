<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function store(Request $request) {

        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'message' => 'required|max:1000',
        ]);
        
        try {
            if (setting('admin.moderate_comments')) {
                $status = 0;
            } else {
                $status = 1;
            }
            
            $comment = new Comment(); 
            $comment->post_id = $request->input('post_id');
            $comment->ip_adress = $request->ip();
            $comment->status = $status;
            $comment->name = $request->input('name');
            $comment->email = $request->input('email');
            $comment->message = $request->input('message');
            $comment->save();
            
            
        } catch (\Exception $e) {
            
            return json_encode(['status' => 401]);
        }
        
        $data = [
            'status' => 200,
            'id' => $comment->id,
            'name' => $comment->name,
            'message' => $comment->message,
            'created_at' => $comment->created_at->format('j, F, Y, H : i'),
            ];
        
        return json_encode($data);
    }
}
