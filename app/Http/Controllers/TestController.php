<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Models\Painter;

class TestController extends Controller
{
    
    public function store(Request $request)
    {

        echo self::getRegion()['area'];
        echo '<br>';
        echo self::getRegion()['city'];
       
    }
    
    public function getRegion() {
        
        $json = file_get_contents('regions.json');
        
        $regions = json_decode($json, 1);
        
        $areas = [
            2121, 2123, 2126, 2134, 2147, 2152, 2155, 2160, 
            2164, 2169, 2173, 2180, 2185, 2188, 2193, 2198, 
            2200, 2204, 2206, 2209, 2212, 2216, 2220, 2224,
        ];
        
        $area = random_int(0, 23);
        
        $city = random_int(0, (count($regions[$areas[$area]]))-1);
        
        echo $areas[$area];
        echo '<br>';
        echo $regions[$areas[$area]][$city];
        
        $result = ['area' => $areas[$area], 'city' => $regions[$areas[$area]][$city]];
        
        //return $result;
    }
  
    public function destroy($id)
    {
        //
    }
}
