<?php
namespace App;

class Search {
    
    public function searchPagesByQuery($modelsSearch, $query)
    {
        $links = [];
        
        foreach ($modelsSearch as $modelName){
             
            $model = 'App\\Models\\'. $modelName;
            
            $searchLists = $model::where($model::searchBy, 
                    'LIKE', '%' . $query . '%')->get();
            
            foreach ($searchLists as $searchList){
                $linksRow['link'] = $model::prefixLink . $searchList[$model::getLink];
                $linksRow['linkText'] = $searchList[$model::getLinkText];
                array_push($links, $linksRow);
            }
        }
        
        return $links;
    }
}
