@extends('layouts.master')
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)
@section('content')
    <div id="content" class="site-content">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="maincont">
                    <div class="cont">
                        <article class="s-post page-styling">
                            <div class="post-info">
                                <a href="{{url('/blog')}}">Блог</a>
                                <h1>{{ $page->title }}</h1>
                                <time><span>{{$page->created_at->format('j')}}</span> {{$page->created_at->format('F')}}</time>
                            </div>

                            <div class="post-video image-post">
                                @if($page->youtube)
                                    <iframe src="https://www.youtube.com/embed/jL9wSGPsdDw?rel=0&amp;controls=0" allowfullscreen></iframe>
                                @else
                                    <img src="{{ Voyager::image($page->image) }}" />
                                @endif
                            </div>

                            {!!$page->body!!}
                            
                            <ul class="post-share">
                                <li>
                                    <a onclick="window.open('https://www.facebook.com/sharer.php?s=100&amp;p[url]=<?php echo $current_url ?>','sharer', 'toolbar=0,status=0,width=620,height=280');" data-toggle="tooltip" title="Share on Facebook" href="javascript:">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a onclick="popUp=window.open('http://twitter.com/home?status=How we scaled up social media <?php echo $current_url ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;" data-toggle="tooltip" title="Share on Twitter" href="javascript:;">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a onclick="popUp=window.open('http://vk.com/share.php?url=<?php echo $current_url ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;" data-toggle="tooltip" title="Share on VK" href="javascript:;">
                                        <i class="fa fa-vk"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tooltip" title="Share on Pinterest" onclick="popUp=window.open('http://pinterest.com/pin/create/button/?url=<?php echo $current_url ?>&amp;description=How we scaled up social media&amp;media=img/1/blog/post1.jpg','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;" href="javascript:;">
                                        <i class="fa fa-pinterest"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tooltip" title="Share on Linkedin" onclick="popUp=window.open('http://linkedin.com/shareArticle?mini=true&amp;url=<?php echo $current_url ?>&amp;title=How we scaled up social media','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;" href="javascript:;">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tooltip" title="Share on Tumblr" onclick="popUp=window.open('http://www.tumblr.com/share/link?url=<?php echo $current_url ?>&amp;name=How we scaled up social media&amp;description=Etiam+mollis+mi+id+nisl+varius+consequat.+In+lacinia+vestibulum+mi%2C+pulvinar+venenatis+justo+sollicitudin+vel.+Pellentesque+et+felis+eget...','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;" href="javascript:;">
                                        <i class="fa fa-tumblr"></i>
                                    </a>
                                </li>
                            </ul>
                            <div class="post-tags" id="post-tag">
                                <a href="#" rel="tag">Авто-эксперт Леонтий Муравьев</a>     
                            </div>
                            <div class="post-comments">
                                <div class="post-addcomment-form" id="post-addcomment-form">
                                    <p class="post-addcomment-ttl">Оставьте ответ</p>
                                    <form action="{{route('comment_store') }}" method="post" id="commentform" class="comment-form form-validate">
                                        @csrf
                                        <input type="hidden" name="post_id" value="{{$page->id}}">
                                        @auth
                                            <input type="hidden" name="name"  value="{{Auth::user()->name}}">
                                            <input type="hidden" name="email" value="{{Auth::user()->email}}">
                                        @else
                                            <input name="name" type="text" data-required="text" placeholder="Имя">
                                            <input type="email" name="email" data-required="text" data-required-email="email" placeholder="E-mail">
                                        @endauth
                                        <textarea name="message" data-required="text" placeholder="Комментарий"></textarea>
                                        <button class="btn1" type="submit"><i class="fa"></i> Отправить</button>
                                        <p class="form-result">Комментарий добавлен!</p>
                                        <p class="error-send">Комментарий не добавлен!</p>
                                    </form>
                                </div>
                                <a href="javascript:void(0);" class="post-comments-add" id="post-comments-add">Добавить комментарий</a>
                                <h3 class="post-comments-ttl">Комментарии на тему "{{ $page->title }}"</h3>
                                <ul class="comment-list">
                                    @foreach($comments as $comment)
                                        <li class="post-comment" id="comment-{{$comment->id}}">
                                            <div class="post-comment-inner">
                                                <p class="post-comment-img">
                                                    <img alt="" src="{{asset('img/comment-icon.jpg')}}">
                                                </p>
                                                <h4 class="post-comment-ttl">{{$comment->name}}</h4>
                                                <p class="comment-meta">
                                                    <span class="created_at">{{$comment->created_at->format('j, F, Y, H : i')}}</span>
                                                    <a class="comment-reply-link" href="javascript:void(0);">Ответить</a>
                                                    <a class="comment-link" href="#comment-{{$comment->id}}">Ссылка</a>
                                                </p>
                                                <div class="post-comment-cont">
                                                    @php
                                                        preg_match_all("/@\w+/", $comment->message, $matches);
                                                        $match = '<span class="reply_name">';
                                                        $match .= implode('', $matches[0]);
                                                        $match .= '</span>';
                                                    @endphp
                                                    <p class="message">{!! str_replace($matches[0], $match, $comment->message)!!}</p>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>          
                            </div>
                        </article>
                    </div>
                </div><!-- .maincont -->
            </main><!-- #main -->
        </div>
    </div><!-- #content -->
@endsection
