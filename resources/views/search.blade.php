@extends('layouts.master')
@section('title', 'Результат поиска')

@section('content')
<div id="content" class="site-content painter">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="cont maincont">
                <h1 class="maincont-ttl">Результат поиска</h1>
                <ul class="b-crumbs">
                    <li><a href="{{url('/')}}">Главная</a></li>
                    <li>Результат поиска</li>
                </ul> 
                <div class="cont row-wrap-boxed">
                    <div class="page-cont">
                        <ul class="results">
                            @if($results->count() > 0)
                                @foreach($results as $result)
                                    <li>
                                        <a href="{{$result['link']}}">{{$result['linkText']}}</a>
                                    </li>
                                @endforeach
                            @else
                                <li>
                                    Ничего не найдено:(
                                </li>
                            @endif
                        </ul>
                    </div>
                    {{ $results->links('vendor.pagination.default') }}
                </div>
            </div>
        </main>
    </div>
</div>
@endsection
