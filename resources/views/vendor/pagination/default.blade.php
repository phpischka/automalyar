@if ($paginator->hasPages())
    <nav>
        <ul class="page-numbers">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li><a class="next page-numbers" href="javascript:void(0);"><i class="fa fa-angle-left"></i></a></li>
            @else
                <li><a class="next page-numbers" href="{{ $paginator->previousPageUrl() }}"><i class="fa fa-angle-left"></i></a></li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @php
                            if(isset($_SERVER['QUERY_STRING'])){
                                $string = $_SERVER['QUERY_STRING'];
                                $str = preg_replace('/\page=[0-9]+&?/','',$string);
                                if($str){
                                    $param = '&' . $str;
                                }else{
                                    $param = $str;
                                }
                            }else{
                                $param = '';
                            }
                            
                        @endphp
                        @if ($page == $paginator->currentPage())
                            <li><span class="page-numbers current">{{ $page }}</span></li>
                        @else
                            <li><a class="page-numbers" href="{{ $url . $param}}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li><a class="next page-numbers" href="{{ $paginator->nextPageUrl() }}"><i class="fa fa-angle-right"></i></a></li>
            @else
                <li><a class="next page-numbers" href="javascript:void(0);"><i class="fa fa-angle-right"></i></a></li>
            @endif
        </ul>
    </nav>
@endif
