@php
//Session::put('key', 'hi Andrey');
# Округление целого числа в большую сторону с коэффициентом
function ceilCoefficient($number, $rate = 50)
{
	// если число дробное, то округляем его до целого
	$number = ceil($number);
	// разделим число на коэффициент, а результат округлим в большую сторону. Потом умножим число на округленный коэффициент
	$rest = ceil($number / $rate) * $rate;
	return $rest;
}
//echo ceilCoefficient(5120, 100);
$arr = ['123' => 'Мфзепы', '678' => 'Ивана Иллича'];
echo $arr['123'];
@endphp
