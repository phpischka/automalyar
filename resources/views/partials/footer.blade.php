@php
        $parent_category = App\Models\Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {
            $categories = false;
        }else{
            $categories = $parent_category->children()->orderBy('order')->get();
        }   
@endphp
<div class="container-fluid blog-sb-widgets page-styling site-footer" id="site-footer">
    <div class="row">
        <div class="col-sm-12 col-md-4 widget align-center-tablet f-logo-wrap">
            <a href="{{url('/')}}" class="f-logo">
                <img src="{{ Voyager::image(setting('site.logo')) }}" alt="logo">
            </a>
            <p>Поддержка Администрации</p>
            <button class="btn callback">Напишите нам</button>
        </div>
        @if($categories)
            @foreach($categories->chunk(5) as $chunk)
            <div class="col-xs-6 col-sm-3 col-md-2 align-center-mobile widget">
                <h3 class="widgettitle">Категория</h3>
                <ul class="menu">
                    @foreach($chunk as $category)
                        <li>
                            <a href="{{url('/pokraska/' .$category->slug)}}">{{$category->name}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
           @endforeach
       @endif
        <div class="col-xs-6 col-sm-3 col-md-2 align-center-mobile widget">
            <h3 class="widgettitle">Страницы</h3>
            <ul class="menu">
                @foreach(App\Models\Page::where('status', 'ACTIVE')->orderBy('order')->get() as $page)
                    <li>
                        <a href="{{url($page->slug)}}">{{$page->title}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<div class="form-validate modal-form" id="modal-form">
    <form action="{{route('send')}}" method="POST" class="form-validate">
        @csrf
        <h4>Напишите нам</h4>
        <input type="text" placeholder="Имя" data-required="text" name="name" />
        <input type="text" placeholder="Телефон" data-required="text" name="phone" />
        <input type="text" placeholder="Email" data-required="text" data-required-email="email" name="email" />
        <textarea name="message" placeholder="Комментарий ..." data-required="text"></textarea> 
        <button class="btn1" type="submit"><i class="fa"></i> Отправить</button>
        <p class="form-result">Сообщение отправлено!</p>
        <p class="error-send">Сообщение не отправлено!</p>
    </form>
</div>
<div class="comment-layout">
    <li class="post-comment" id="">
        <div class="post-comment-inner">
            <p class="post-comment-img">
                <img alt="" src="{{asset('img/comment-icon.jpg')}}" />
            </p>
            <h4 class="post-comment-ttl"></h4>
            <p class="comment-meta">
                <span class="created_at"></span>
                <a class="comment-reply-link" href="javascript:void(0);">Ответить</a>
                <a class="comment-link" href="">Ссылка</a>
            </p>
            <div class="post-comment-cont">
                <p class="message"></p>
            </div>
        </div>
    </li>
</div>
<div class="review-layout" style="display: none">
    <li class="prod-review" id="">
        <h3 class="name"></h3>
       <p class="prod-review-rating" title="">
          <i class="rating-ico"></i>
          <time class="created_at"></time>
          <a class="review-link" href="">
              <time class="created_at"></time>
          </a>
       </p>
       <p class="description"></p>
    </li>
</div>
<div class="container-fluid page-styling site-header-before footer-copyright">
   <div class="row">
      <div class="col-12">
         <p class="copyright">©2021 <a href="{{url('/')}}">AutoBrillar</a> Все права защищены.</p>
      </div>
   </div>
</div>

