<div class="blog-sb-widget multishopfeaturedproducts_widget">
    <h3 class="widgettitle">Последние статьи</h3>
    <div class="products-featured-wrap">
        @foreach($posts as $post)
            <div class="products-featured">
                <a href="{{url('/post/' .$post->slug)}}" class="blog-img">
                    <img src="{{ Voyager::image($post->image) }}" alt="">
                </a>
                <h5 class="products-featured-ttl"><a href="{{url('/post/' .$post->slug)}}">{{$post->title}}</a></h5>
            </div>
        @endforeach
    </div>
</div>