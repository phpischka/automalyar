<div class="container-fluid page-styling site-header-before">
    <div class="row">
        <div class="col-lg-4">
            <ul class="links_list links_list-align-left align-center-desktop topbar-social">
                <li>
                    <p class="links_list-value">
                        <a href="{{setting('site.facebook')}}" target="_blank" rel="nofollow">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </p>
                </li>
                <li>
                    <p class="links_list-value">
                        <a href="mailto:{{setting('site.email')}} target="_blank" rel="nofollow">
                            <i class="fa fa-paper-plane"></i>
                        </a>
                    </p>
                </li>
                <li>
                    <p class="links_list-value">
                        <a href="{{setting('site.pinterest')}}" target="_blank" rel="nofollow">
                            <i class="fa fa-pinterest-p"></i>
                        </a>
                    </p>
                </li>
                @if(setting('site.youtube'))
                <li>
                    <p class="links_list-value">
                        <a href="{{setting('site.youtube')}}" target="_blank" rel="nofollow">
                            <i class="fa fa-youtube-play"></i>
                        </a>
                    </p>
                </li>
                @endif
                <li>
                    <p class="links_list-value">
                        <a href="{{setting('site.twitter')}}" target="_blank" rel="nofollow">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </p>
                </li> 
                <li>
                    <p class="links_list-value">
                        <a href="{{setting('site.instagram')}}" target="_blank" rel="nofollow">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </p>
                </li>
            </ul>
        </div>
        <div class="col-lg-8">
            <ul class="links_list links_list-align-right align-center-desktop topbar-contacts">
                <li>
                    <p class="links_list-label">
                        Our Address
                    </p>
                    <p class="links_list-value">
                        <a href="http://maps.google.com" target="_blank" rel="nofollow">15th Street, Miami, USA</a>
                    </p>
                </li>
                <li>
                    <p class="links_list-label">
                        Contact Us
                    </p>
                    <p class="links_list-value">
                        <a href="mailto:support@email.com">Support@Email.com</a>
                    </p>
                </li>
                <li>
                    <p class="links_list-label">
                        Phone
                    </p>
                    <p class="links_list-value">
                        <a href="tel:4785929899">(478)-592-9899</a>
                    </p>
                </li>
            </ul>
        </div>
    </div>
</div>