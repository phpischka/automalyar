@extends('layouts.master')
@section('title', 'Кабинет АвтоМаляра')
@section('content')
<div id="content" class="site-content cabinet">
   <div id="primary" class="content-area width-normal">
      <main id="main" class="site-main">
         <div class="cont maincont">
            <h1 class="maincont-ttl">Кабинет АвтоМаляра</h1>
            <article class="page-cont">
               <div class="page-styling">
                  <div class="woocommerce">
                    @if ($entry)
                        <div class="woocommerce-info">
                            <div class="info-cabinet-off">
                               <i class="fa fa-phone"></i> Количество просмотров <a class="showcoupon">{{ $entry->phone_view ?? '0'}}</a>
                            </div>
                        </div>
                    @endif
                     <div class="mb55 promobox-i promobox-i-hasimg promobox-i-hasbtn">
                        <p class="promobox-i-icon"><i class="fa fa-list-ol logo"></i></p>
                        <h3>Объявление</h3>
                        <p>Заполняйте корректно контактную информацию по которым клиенты смогут обратиться к Вам по адресу или по телефону.<br>
                            Указывайте только актуальные цены, чтобы клиентам было проще выбирать автомаляра и писать хорошие отзывы, в том числе, и на нашем сервисе.
                        </p>.
                        @if (!$entry)
                            <a class="promobox-i-link" href="{{url('/cabinet/advt/create')}}">Создать</a>
                        @else
                            <a class="promobox-i-link" href="{{url('/cabinet/advt/edit/' . $entry->id)}}">Редактировать</a>
                        @endif
                    </div>
                  </div>
               </div>
            </article>
         </div>
      </main>
      <!-- #main -->
   </div>
   <!-- #primary -->    
</div>
@endsection