@extends('layouts.master')
@push ('scripts')
<script type="text/javascript">
    let dataList = {};
    fetch("{{url('/')}}/Json/city-ua.json").then(response => {
        return response.json()
    }).then(
            json => {
                dataList = json;
                json.areas.forEach(area => {
                    var opt = document.createElement('option');
                    opt.value = area.id;
                    opt.innerHTML = area.name;
                    billing_area.appendChild(opt);
                }
                );
            }
    );
            
    billing_area.addEventListener('change', () => {
        billing_city.innerHTML = '';
        dataList.areas.filter(item => item.id == billing_area.options[billing_area.selectedIndex].value)[0].areas.forEach(
                area => {
                    var opt = document.createElement('option');
                    opt.value = area.id;
                    opt.innerHTML = area.name;
                    billing_city.appendChild(opt);
                }
        )

    });
    //Upload Image
    const input = document.getElementById('billing_image');

    input.addEventListener('change', (event) => {
        const target = event.target;
        if (target.files && target.files[0]) {
            const maxAllowedSize = 2 * 1024 * 1024;
            if (target.files[0].size > maxAllowedSize) {
                alert('Допустимый размер картинки не более 2МБ');
                target.value = '';
          }
      }
    })
</script>
@endpush
@section('title', 'Кабинет АвтоМаляра')
@section('content')
<div id="content" class="site-content">
    <div id="primary" class="content-area width-normal">
        <main id="main" class="site-main">
            <div class="cont maincont">
                <h1 class="maincont-ttl">Кабинет АвтоМаляра</h1>
                <article class="page-cont">
                    <div class="page-styling">
                        <div class="woocommerce">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (session('success'))
                                <div class="alert alert-green">
                                    {{ session('success')}}
                                </div>
                            @endif
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error')}}
                                </div>
                            @endif
                            <form method="post" class="checkout woocommerce-checkout" action="{{url('/cabinet/advt/store')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="col2-set" id="customer_details">
                                    <div class="col-1">
                                        <div class="woocommerce-billing-fields">
                                            <h3>Контактные данные (Вашего СТО/Мастерской)</h3>
                                            <div class="woocommerce-billing-fields__field-wrapper">
                                                <p class="form-row form-row-first validate-required" id="billing_first_name_field" data-priority="10">
                                                    <label for="billing_first_name" class="">Имя <abbr class="required" title="required">*</abbr></label>
                                                    <input type="text" class="input-text " name="first_name" id="billing_first_name" placeholder="" value="{{old('first_name') ?? $user->name}}" autocomplete="given-name" autofocus="autofocus" required="">
                                                </p>
                                                <p class="form-row form-row-last validate-required" id="billing_last_name_field" data-priority="20">
                                                    <label for="billing_last_name" class="">Фамилия <abbr class="required" title="required">*</abbr></label>
                                                    <input type="text" class="input-text " name="last_name" id="billing_last_name" placeholder="" value="{{old('last_name')}}" autocomplete="family-name" required="">
                                                </p>
                                                <p class="form-row form-row-wide" id="billing_company_field" data-priority="30">
                                                    <label for="billing_company" class="">Название Компании ("ТОВ Автопокраска") / Частный предприниматель ("ФОП Петров А.А") <abbr class="required" title="required">*</abbr></label>
                                                    <input type="text" class="input-text " name="company" id="billing_company" placeholder="" value="{{old('company')}}" autocomplete="organization" required="">
                                                </p>
                                                <p class="form-row form-row-wide" id="billing_about_field" data-priority="30">
                                                    <label for="billing_about" class="">О Компании / Частном предпринимателе (максимум 255 символов) <abbr class="required" title="required">*</abbr></label> 
                                                    <textarea class="input-text " name="about" id="billing_about" rows="3" maxlength="255" required>{{old('about')}}</textarea>
                                                </p>
                                                <p class="form-row form-row-wide" id="billing_site_field" data-priority="30">
                                                    <label for="billing_site" class="">Сайт <abbr></abbr></label>
                                                    <input type="text" class="input-text " name="site" id="billing_site" placeholder="Не обязательно" value="{{old('site')}}" autocomplete="organization">
                                                </p>
                                                <p class="form-row form-row-wide address-field update_totals_on_change validate-required" id="billing_country_field" data-priority="40">
                                                    <label for="billing_country" class="">Страна
                                                        <abbr class="required" title="required">*</abbr></label>
                                                    <select name="country" id="billing_country" class="country_to_state country_select " autocomplete="country">
                                                        <option value="ukraine">Украина</option>
                                                    </select>
                                                    <noscript>
                                                    <input type="submit5" name="woocommerce_checkout_update_totals" value="Update country"/>
                                                    </noscript>
                                                </p>
                                                <p class="form-row form-row-wide address-field update_totals_on_change validate-required" id="billing_city_field" data-priority="40">
                                                    <label for="billing_country" class="">Город
                                                        <abbr class="required" title="required">*</abbr></label>
                                                    <select name="area" id="billing_area" class="country_to_state city_select " autocomplete="city" >
                                                        <option value="{{old('area')}}">Выбрать область</option>
                                                    </select>
                                                    <select name="city" id="billing_city" class="country_to_state city_select " autocomplete="city" required>
                                                        <option value="{{old('city')}}"">Выбрать город</option>
                                                    </select>
                                                </p>
                                                <p class="form-row form-row-wide address-field validate-required" id="billing_address_1_field" data-priority="50">
                                                    <label for="billing_address_1" class="">Улица <abbr class="required" title="required">*</abbr></label>
                                                    <input type="text" class="input-text " name="street" id="billing_address_1" placeholder="" value="{{old('street')}}" autocomplete="address-line1" required="">
                                                </p>
                                                <p class="form-row form-row-wide address-field" id="billing_address_2_field" data-priority="60">
                                                    <label for="billing_address_1" class="">Номер здания <abbr class="required" title="required">*</abbr></label>
                                                    <input type="text" class="input-text " name="house" id="billing_address_2" placeholder="" value="{{old('house')}}" autocomplete="address-line2" required="">
                                                </p>
                                                <p class="form-row form-row-first validate-required validate-phone" id="billing_phone_field" data-priority="100">
                                                    <label for="billing_phone" class="">Телефон <abbr class="required" title="required">*</abbr></label>
                                                    <input type="tel" class="input-text " name="phone" id="billing_phone" placeholder="" value="{{old('phone')}}" autocomplete="tel">
                                                </p>
                                                <p class="form-row form-row-last validate-required validate-email" id="billing_email_field" data-priority="110">
                                                    <label for="billing_email" class="">Email <abbr class="required" title="required">*</abbr></label>
                                                    <input type="email" class="input-text " name="email" id="billing_email" placeholder="" autocomplete="email username" value="{{old('email') ?? $user->email}}">
                                                </p>
                                                <p class="form-row form-row-first validate-required validate-image" id="billing_image_field" data-priority="100">
                                                    <label for="billing_image" class="">Фото вашей мастерской (рекомендовано одинаковой пропорции, к примеру, 400*400px не более 2МБ)</label>
                                                    <input type="file" class="input-text " name="image" id="billing_image">
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h3 id="order_review_heading">Стоимость услуги покраски</h3>
                                <p class="note">Важно: Элемент кузова (деталь) для которой не выполняеете покраску, оставляйте пустым.</p>
                                <div id="order_review" class="woocommerce-checkout-review-order">
                                    <table class="shop_table woocommerce-checkout-review-order-table">
                                        <thead>
                                            <tr>
                                                <th class="product-name">Деталь <abbr class="required" title="required">*</abbr></th>
                                                <th class="product-total">Начальная Цена (грн)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($categories as $category)
                                            <tr class="cart_item">
                                                <td class="product-name">
                                                    {{$category->name}}
                                                </td>
                                                <td class="product-total">
                                                    <input type="number" class="input-text " name="{{$category->input_name_form}}" min="1">
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <p class="notes">На заметку: Если какого то поля по "Услуге покраски" не хватает напишите пожелание на его добавление в поддержку <a class="callback" href="">Администрации</a></p>
                                    <input type="checkbox" required="">
                                    <label class="consent">Согласие на обработку персональных данных <a href="{{route('terms')}}">Условия использования</a> <abbr class="required" title="required">*</abbr></label>
                                    <div id="payment" class="woocommerce-checkout-payment">
                                        <div class="form-row place-order">
                                            <button class="btn-save" type="submit"><i class="fa "></i> Сохранить</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </article>
            </div>
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->    
</div>
@endsection