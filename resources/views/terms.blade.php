@extends('layouts.master')
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)
@section('content')
<div id="content" class="site-content painter">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="cont maincont">
                <h1 class="maincont-ttl">{{$page->title}}</h1>
                <ul class="b-crumbs">
                    <li><a href="{{url('/')}}">Главная</a></li>
                    <li>{{$page->title}}</li>
                </ul> 
                <div class="cont row-wrap-boxed">
                    <div class="page-cont">
                        <p class="text-page">{!!$page->body!!}</p>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
@endsection
