@extends('layouts.master')

@section('content')
<div id="content" class="site-content">
   <div id="primary" class="content-area width-normal">
      <main id="main" class="site-main">
         <div class="cont maincont">
            
            <article class="page-cont">
               <div class="page-styling">
                  <div class="auth-wrap">
                     <div class="auth-col">
                        <h2>Сброс пароля</h2>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="post" class="login" action="{{ route('password.email') }}">
                            @csrf
                           <p>
                              <label for="username">E-mail <span class="required">*</span></label>
                              <input type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus id="username">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           </p>
                           <p class="auth-submit">
                              <input type="submit" value="Отправить ссылку для сброса пароля">
                           </p>
                        </form>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </main>
      <!-- #main -->
   </div>
   <!-- #primary -->    
</div>
<!-- #content -->
@endsection
