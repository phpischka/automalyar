<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PainterController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\SitemapController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [PageController::class, 'index']);

Route::get('/t', [TestController::class, 'store']);


//painting
Route::get('pokraska', [PageController::class, 'painting'])->name('painting');
Route::get('/pokraska/{slug}', [PageController::class, 'painting_category']);
//Route::get('/pokraska/{slug}', [PageController::class, 'painting_category']);

Route::get('search', [PageController::class, 'search'])->name('search');
//contact
Route::post('/send', [ContactController::class, 'send'])->name('send');
Route::post('/send_partner', [ContactController::class, 'send_partner'])->name('send_partner');


//reviews
Route::post('/review', [ReviewController::class, 'store'])->name('review');
//master
Route::get('/avtomalyar/{id}', [PageController::class, 'painter']);
Route::get('/avtomalyar/{id}/otzyvy', [PageController::class, 'review_painter']);

//Blog
Route::get('/blog', [PageController::class, 'blog'])->name('blog');
Route::get('/post/{slug}', [PageController::class, 'post']);
Route::post('/comment-store', [CommentController::class, 'store'])->name('comment_store');

Route::get('/usloviya-ispolzovaniya', [PageController::class, 'terms'])->name('terms');

Route::get('karta-sajta', [SitemapController::class, 'sitemap'])->name('sitemap');
Route::get('sitemap.xml', [SitemapController::class, 'index']);

//Profile
Route::get('/cabinet', [PainterController::class, 'index']);

Route::get('/cabinet/advt/create', [PainterController::class, 'create']);
Route::post('/cabinet/advt/store', [PainterController::class, 'store']);
Route::post('/getPhone', [PageController::class, 'getPhone']);

Route::get('/cabinet/advt/edit/{id}', [PainterController::class, 'edit']);
Route::post('/cabinet/advt/update/{id}', [PainterController::class, 'update']);

Auth::routes();
Route::group(['prefix' => 'magistr'], function () {
    Voyager::routes();
});

